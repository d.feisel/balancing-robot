from agent import Agent
import gym
import gym_foo

import tensorflow as tf


# falls das standard environment benutzt werden soll:
# env = gym.make("CartPole-v0")
# spec = gym.spec("CartPole-v0")

# custom environment
env = gym.make("gym_foo:foo-v0")
spec = gym.spec("gym_foo:foo-v0")
env.env.__init__(g=10., m_c=0.53, m_p=0.35, l_p=0.1, t=0.02, start_var=0.05, f_mag=10)

# soll trainiert, getestet oder gespeichert werden? das mit 1 ausgewählte modul wird ausgeführt, die anderen nicht
train = 0
test = 0
save = 1


num_episodes = 400
graph = True

# Messrauschen und Störung
noise = 0.0



# laden eines gespeicherten Netzwerks für das testen oder speichern
file_type = 'tf'
# wenn die Datei nicht existiert, muss zunächst ein Netzwerk trainiert werden und hier der Dateipfad angegeben werden
file = 'saved_networks/Netzwerk_8_robotparams_01'

# Datei für die Gewichte für Übertragung auf Arduino
weightfile = "weights.txt"

# initialisieren des Agenten
dqn_agent = Agent(lr=0.001, discount_factor=0.99, num_actions=2, epsilon=1, batch_size=64, input_dims=4)



if train and not test and not save:
    dqn_agent.train_model(env, num_episodes, graph, noise)
elif save and not test and not train:
    dqn_agent.saveweights(file_type,file, weightfile)
else:
    dqn_agent.test(env, num_episodes, file_type, file, graph, noise)
