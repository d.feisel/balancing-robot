import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import time

from tensorflow.python.keras import Sequential
from tensorflow.python.keras.layers import Dense
from tensorflow.keras.optimizers import Adam

from replay_buffer import ReplayBuffer


def DeepQNetwork(lr, num_actions, input_dims, fc1, fc2):
    q_net = Sequential()
    q_net.add(Dense(fc1, input_dim=input_dims, activation='relu'))
    q_net.add(Dense(fc2, activation='relu'))
    q_net.add(Dense(num_actions, activation=None))
    q_net.compile(optimizer=Adam(learning_rate=lr), loss='mse')

    return q_net


class Agent:
    def __init__(self, lr, discount_factor, num_actions, epsilon, batch_size, input_dims):
        self.action_space = [i for i in range(num_actions)]
        self.discount_factor = discount_factor
        self.epsilon = epsilon
        self.batch_size = batch_size
        # exponentieller Abfall für epsilon eingefügt (war vorher linear)
        self.epsilon_decay = 0.9995
        self.epsilon_final = 0.02
        self.update_rate = 100
        self.step_counter = 0
        self.buffer = ReplayBuffer(1000000, input_dims)
        self.q_net = DeepQNetwork(lr, num_actions, input_dims, 8,8) #änderung der Größe der hidden layers, ursprünglich 256, 256
        self.q_target_net = DeepQNetwork(lr, num_actions, input_dims, 8,8)

        # für validation stop eingefügt
        self.max_avg = 350
        self.i_save = 0

    def store_tuple(self, state, action, reward, new_state, done):
        self.buffer.store_tuples(state, action, reward, new_state, done)

    def policy(self, observation):
        if np.random.random() < self.epsilon:
            action = np.random.choice(self.action_space)
        else:
            state = np.array([observation])
            actions = self.q_net(state)
            action = tf.math.argmax(actions, axis=1).numpy()[0]

        return action

    def train(self, score):
        if self.buffer.counter < self.batch_size:
            return
        if self.step_counter % self.update_rate == 0:
            self.q_target_net.set_weights(self.q_net.get_weights())

        state_batch, action_batch, reward_batch, new_state_batch, done_batch = \
            self.buffer.sample_buffer(self.batch_size)

        q_predicted = self.q_net(state_batch)
        q_next = self.q_target_net(new_state_batch)
        q_max_next = tf.math.reduce_max(q_next, axis=1, keepdims=True).numpy()
        q_target = np.copy(q_predicted)

        for idx in range(done_batch.shape[0]):
            target_q_val = reward_batch[idx]
            if not done_batch[idx]:
                target_q_val += self.discount_factor*q_max_next[idx]
            q_target[idx, action_batch[idx]] = target_q_val
        self.q_net.train_on_batch(state_batch, q_target)

        # Berechnung des epsilon abfalls
        self.epsilon = self.epsilon * self.epsilon_decay if self.epsilon > self.epsilon_final else self.epsilon_final
        self.step_counter += 1

    def train_model(self, env, num_episodes, graph, noise):
        before = time.perf_counter()
        scores, episodes, avg_scores, obj, times, validation_times, validation_scores = [], [], [], [], [], [], []
        t_stop = 0
        score_stop = 0
        goal = 495
        stop = 0
        f = 0
        txt = open("saved_networks.txt", "w")

        for i in range(num_episodes):
            """
            # keine random entscheidungen mehr, wenn es erfloge hatte --> bringt keine verbesserung
            #if (i > 4) and (sum(scores[-4:]) > 195 * 4):
            #    self.epsilon = 0
            #    self.epsilon_final = 0
            #if (i > 1) and (scores[-1] > 190):
            #    self.epsilon = self.epsilon_final #klappt nicht so wie gedacht
            """
            done = False
            score = 0.0
            state = env.reset()

            while not done:
                #env.render() # auskommentieren, wenn lernen auch dargestellt werden soll

                # berechne normalverteiltes Messrauschen des Zustandsvektors
                pseudostate = state+noise*(np.random.randn(4))
                action = self.policy(pseudostate)

                new_state, reward, done, _ = env.step(action)
                score += reward
                self.store_tuple(state, action, reward, new_state, done)
                state = new_state
                self.train(score)

            scores.append(score)
            obj.append(goal)
            episodes.append(i)
            avg_score = np.mean(scores[-5:])#-100
            avg_scores.append(avg_score)
            after = time.perf_counter()
            zeit = after - before
            times.append(zeit)
            print("Episode {:.0f}/{:.0f}\t Time {:.1f} \t Score: {:.1f} \t Epsilon: {:.3f} \t AVG Score: {:.1f} \t Disturbance: {:.0f} + {:.0f}, {:.0f} "
                  .format(i, num_episodes, zeit, score, self.epsilon, avg_score,a,b,s))

            # Validation Stop --> Automatisches Speichern und Abbrechen des Trainings
            if score > self.max_avg:
                self.max_avg = score #avg_score
                self.q_net.save(("saved_networks/dqn_model{0}".format(f)))
                self.q_net.save_weights(("saved_networks/dqn_model{0}/net_weights{0}.h5".format(f)))
                txt.write("Save {0} - Episode {1}/{2}, Score: {3} ({4}), AVG Score: {5}\n".format(f, i, num_episodes,
                                                                                                  score, self.epsilon,
                                                                                                  avg_score))
                f += 1
                print("Network saved")
                self.i_save = i
                # speichern wann und bei welchem score gespeichert wurde für plot
                validation_times.append(zeit)
                validation_scores.append(avg_score)

            # Abbruchbedingung
            if i>self.i_save+5 and  len(avg_scores) > 20 and np.mean(avg_scores[-5:]) <= self.max_avg and self.max_avg != 350:
                print(i,"\t",round(zeit,2),"\t",round(self.max_avg,2),"\t",round(np.max(avg_scores),2))
                break

        print(num_episodes,"\t",round(zeit,2),"\t",round(np.max(scores),2),"\t",round(np.max(avg_scores),2))
        txt.close()

        # Erstellung eines Plots des Trainings
        if graph:
            df = pd.DataFrame({'x': episodes, 'Score': scores, 'Average Score': avg_scores, 'Solved Requirement': obj, 't': times})
            df.to_csv('val_data1.txt')
            validation_times.append(t_stop)
            validation_scores.append(score_stop)
            df2 = pd.DataFrame({'val t': validation_times, 'val Score': validation_scores})
            df2.to_csv('val_data2.txt')
            #plt.plot('x', 'Score', data=df, marker='', color='blue', linewidth=2, label='Score')
            plt.scatter('t', 'Average Score', data=df, marker='+', color='orange',label='Average Score')
            plt.scatter(validation_times,validation_scores, marker='+', color='blue',label='Speicherungspunkte')
            plt.scatter(t_stop,score_stop,marker='+', color='red',label='Validation Stop Punkt')
            #plt.plot('x', 'Solved Requirement', data=df, marker='', color='red', linewidth=2, linestyle='dashed',
            #         label='Solved Requirement')
            plt.hlines(350,0,300, linestyle = 'dotted', label = 'Mindestscore')
            plt.xlabel("Zeit in s",fontsize=15)
            plt.ylabel("Score",fontsize=15)
            plt.legend()
            plt.savefig('CartPole_Train.pdf',dpi = 600)

    def test(self, env, num_episodes, file_type, file, graph, noise):
        if file_type == 'tf':
            self.q_net = tf.keras.models.load_model(file)
        elif file_type == 'h5':
            self.train_model(env, 5, False)
            self.q_net.load_weights(file)
        self.epsilon = 0.0
        scores, episodes, avg_scores, obj = [], [], [], []
        goal = 495
        score = 0.0
        for i in range(num_episodes):
            state = env.reset()
            done = False
            episode_score = 0.0

            # Variablen für Störung
            a = 100 # starte Störung in der 100. Episode
            b = 5 # Störung dauert 5 Episoden
            s = (5*i+80)*np.pi/180 # Stärke der Störung
            while not done:
                if a <= episode_score <= a+b:
                    state[3] += s # Änderung der Winkelgeschwindigkeit um Stoß zu simulieren

                state += noise*(np.random.randn(4))

                env.render() # Darstellen der Simulation
                action = self.policy(state)
                new_state, reward, done, _ = env.step(action)
                episode_score += 1 #reward
                state = new_state

                # wenn das Pendel 100 Episoden nach dem Stoß noch nicht umgefallen ist, gilt der Test als bestanden
                if episode_score > a + 100:
                    print("survived ",(80+5*i),"degrees per sec")

            score += episode_score
            scores.append(episode_score)
            obj.append(goal)
            episodes.append(i)
            avg_score = np.mean(scores[-100:])
            avg_scores.append(avg_score)
            if episode_score < a +100: print("failed")
            print(a,s)
            print(episode_score)

        # erstelle Plot zum verlauf des Tests
        if graph:
            df = pd.DataFrame({'x': episodes, 'Score': scores, 'Average Score': avg_scores, 'Solved Requirement': obj})

            plt.plot('x', 'Score', data=df, marker='', color='blue', linewidth=2, label='Score')
            plt.plot('x', 'Average Score', data=df, marker='', color='orange', linewidth=2, linestyle='dashed',
                     label='AverageScore')
            plt.plot('x', 'Solved Requirement', data=df, marker='', color='red', linewidth=2, linestyle='dashed',
                     label='Solved Requirement')
            plt.legend()
            plt.savefig('CartPole_Test.png')

        env.close()


    # Funktion zum Speichern der weights und biases eines trainierten Neuronalen Netzes für den Arduino
    def saveweights(self,file_type,file,weightfile,neuron_lim=16):
        self.epsilon = 0
        if file_type == 'tf':
            self.q_net = tf.keras.models.load_model(file)
        elif file_type == 'h5':
            self.train_model(env, 5, False)
            self.q_net.load_weights(file)

        f = open(weightfile,"w")


        # sanity check zum vergleich mit Berechnungen des Arduinos zum Sicherstellen ob dieser die Q werte korrekt berechnet
        print(self.q_net(np.array([[1,1,1,1]])))


        # manuelle berechnung der Q-Werte, um richtiges Vorgehen für Arduino zu überprüfen
        """
        test_state = np.array([0,0,0,0])
        
        w1 = np.array(self.q_net.layers[0].get_weights()[0])
        b1 = np.array(self.q_net.layers[0].get_weights()[1])
        print(w1.shape,test_state.shape)
        x1 = np.matmul(w1.transpose(),test_state) + b1
        x1 = np.array([0 if x1[i]<0 else x1[i] for i in range(len(x1))])
        print(x1)

        w2 = np.array(self.q_net.layers[1].get_weights()[0])
        b2 = np.array(self.q_net.layers[1].get_weights()[1])
        print(w2.shape, x1.shape)
        x2 = np.matmul(w2.transpose(), x1) + b2
        x2 = np.array([0 if x2[i] < 0 else x2[i] for i in range(len(x2))])
        print(x2)

        w3 = np.array(self.q_net.layers[2].get_weights()[0])
        b3 = np.array(self.q_net.layers[2].get_weights()[1])
        print(w3.shape, x2.shape)
        x3 = np.matmul(w3.transpose(), x2) + b3
        print(x3)
        """

        # Speichern der weights und biases in Format, das direkt in Arduino Programm reinkopiert werden kann
        f.write("int n[2] = {%g, %g}; \n"%(len((self.q_net.layers[0].get_weights()[1])),len((self.q_net.layers[1].get_weights()[1]))))
        i = 0
        for layer in self.q_net.layers:
            i += 1
            #f.write("%s \n"%layer.get_config())
            w = np.array(layer.get_weights()[0]).transpose().ravel()
            b = np.array(layer.get_weights()[1])
            if len(w) > 4 * neuron_lim:
                print("watch out, this network is very large")
            f.write("double w{}[{}] = ".format(i,len(w)))
            f.write("{")
            [f.write("%g, " %w[i]) if i != len(w)-1 else f.write("%g }; \n" %w[i]) for i in range(len(w))]
            f.write("double b{}[{}] = ".format(i,len(b)))
            f.write("{")
            [f.write("%g, " %b[i]) if i != len(b)-1 else f.write("%g }; \n" %b[i]) for i in range(len(b))]


        f.close()

