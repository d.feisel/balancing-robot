
import matplotlib.pyplot as plt
import gym
from matplotlib import cm
import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import gym_foo




def MSE_func(P,I,D, noise_PID):
    env = gym.make("gym_foo:foo-v0")  #Erstellen einer Simulationsumgebung

    desired_state = np.array([0, 0, 0, 0])  # gewünschter Zustands
    # Gewichtung des Zustandsvektors bei Berechnung des Fehlers
    desired_mask = np.array([0.02, 0.05, 0.88, 0.05])

    time = 500 # Anzahl der Zeitschritte in einer Episode
    N = 100 #Anzahl der Episoden
    MSE=np.zeros(N) #Array, in dem der mittlere Quadratische Fehler jeder Episoden gespeichert wird



    for i_episode in range(N):
        avg_SE=np.zeros(time) #Array, in dem der Quadratrische Fehler für alle Zeitschritte
        #einer Episoden gespeichert wird.
        state = env.reset()
        #Mithilfe der Variable Abbruch wird festgestellt, ob das Cartpole die 500 Zeitschritte
        # einer Episode balancieren kann
        abbruch = False

        integral = 0
        derivative = 0
        prev_error = 0

        for t in range(time):
            #env.render() #Graphische Darstellung der Simulation
            #Simulation des Messrauschens: Normalverteilte Änderung der Komponenten des Zustandsvektors
            #mit Standardabweichung noise_PID
            state+=noise_PID*np.random.randn(4)
            error = (state - desired_state)
            avg_SE[t]=0
            #Berechung der Quadartischen Abweichung
            for i in range(4):
                avg_SE[t]+=error[i]**2*desired_mask[i]

            #Berechung von Integral und Ableitung des Fehler mit Euler-Integration und Differenzenquotient
            #env.tau ist der Zeitschritt der Simulation
            integral += error*env.tau
            derivative = (error - prev_error)/env.tau
            prev_error = error

            #Berechung der Kraft, die den Wagen beschleunigt
            force = np.dot(P * error + I * integral + D * derivative, desired_mask)
            #Beschleunigung des Wagens mit der gewünschten Kraft
            state, reward, done, info = env.step(force)


            if done: #feststellen, ob das Cartpole mit den gegeben PID-Werten 500 Zeitschritte balanciert
                if t < (time-2):
                   abbruch = True
                state=env.reset()
                break

            if abbruch == True:
                MSE[i_episode] = 0.5
            else:
                MSE[i_episode] = np.mean(avg_SE)
    env.close()
    print("MSE:", MSE)
    #Mittelwert und Standardabweichung über alle Zeitschritte und Episoden zurückgegen
    return np.mean(MSE), np.std(MSE)








#Intervalle für PID-Werte festlegen, in denen MSE_func berechnet wird
start = [5,0,0]
stop = [20,30,0.20]
#Schrittweite innerhalb der Intervalle
N=8
#Messrauschen für die Simulation
noise_PID=0.01
P=np.linspace(start[0],stop[0],N)
I=np.linspace(start[1],stop[1],N)
D=np.linspace(start[2],stop[2],N)

#Matrix anlegen, in der der MSE gespeichert wird
err_matrix=np.zeros((N,N,N))

#MSE für alle PID-Werte berechnen
for i in range(len(P)):
    for j in range(len(I)):
        for k in range(len(D)):
            err, std = MSE_func(P[i],I[j],D[k], noise_PID)
            err_matrix[i][j][k]=err

#Graphische Darstellung der Ergebnisse in 3D-Plot
X, Y, Z = np.mgrid[start[0]:stop[0]:N*1j, start[1]:stop[1]:N*1j, start[2]:stop[2]:N*1j]
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
scat = ax.scatter(X, Y, Z, c=np.log(err_matrix.flatten()), alpha=1, linewidth=0,s=30)
cbar = fig.colorbar(scat, shrink=0.5, aspect=5)
cbar.set_label('ln(MSE)',fontsize = 15)
ax.set_xlabel('P',fontsize = 15)
ax.set_ylabel('I',fontsize = 15)
ax.set_zlabel('D',fontsize = 15)
ax.view_init(elev=15., azim=8.)
plt.savefig("3d_grid_search.png")
plt.show()

"""
#Graphische Darstellung der Ergebnisse in 2D Plot

err_matrix=np.zeros((N,N))
X, Y = np.ogrid[start[0]:stop[0]:N*1j, start[1]:stop[1]:N*1j]


fig=plt.figure()
ax=fig.add_subplot(projection='3d')
surf = ax.plot_surface(X,Y,err_matrix, cmap=cm.viridis, linewidth=0, antialiased=False)
fig.colorbar(surf, shrink=0.5, aspect=5)
ax.set_xlabel('P')
ax.set_ylabel('D')
ax.set_zlabel('MAE')

plt.savefig("2d_grid_search.png")
plt.show()
"""
