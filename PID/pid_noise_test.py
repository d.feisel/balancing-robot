import numpy as np
import math

import gym
import gym_foo


def MSE_func(P,I,D, noise_PID):
    env = gym.make("gym_foo:foo-v0")  #Erstellen einer Simulationsumgebung

    desired_state = np.array([0, 0, 0, 0])  # gewünschter Zustands
    # Gewichtung des Zustandsvektors bei Berechnung des Fehlers
    desired_mask = np.array([0.02, 0.05, 0.88, 0.05])

    time = 500 # Anzahl der Zeitschritte in einer Episode
    N = 50 # Test der Disturbance für 50 Episoden
    MSE=np.zeros(N) #Array, in dem der mittlere Quadratische Fehler jeder Episoden gespeichert wird



    for i_episode in range(N):
        avg_SE=np.zeros(time) #Array, in dem der Quadratrische Fehler für alle Zeitschritte
        #einer Episoden gespeichert wird.
        state = env.reset()
        #Mithilfe der Variable Abbruch wird festgestellt, ob das Cartpole die 500 Zeitschritte
        # einer Episode balancieren kann
        abbruch = False

        integral = 0
        derivative = 0
        prev_error = 0

        for t in range(time):
            #env.render() #Graphische Darstellung der Simulation
            #Simulation des Messrauschens: Normalverteilte Änderung der Komponenten des Zustandsvektors
            #mit Standardabweichung noise_PID
            state+=noise_PID*np.random.randn(4)
            error = (state - desired_state)
            avg_SE[t]=0
            #Berechung der Quadartischen Abweichung
            for i in range(4):
                avg_SE[t]+=error[i]**2*desired_mask[i]

            #Berechung von Integral und Ableitung des Fehler mit Euler-Integration und Differenzenquotient
            #env.tau ist der Zeitschritt der Simulation
            integral += error*env.tau
            derivative = (error - prev_error)/env.tau
            prev_error = error

            #Berechung der Kraft, die den Wagen beschleunigt
            force = np.dot(P * error + I * integral + D * derivative, desired_mask)
            #Beschleunigung des Wagens mit der gewünschten Kraft
            state, reward, done, info = env.step(force)


            if done: #feststellen, ob das Cartpole mit den gegeben PID-Werten 500 Zeitschritte balanciert
                if t < (time-2):
                   abbruch = True
                state=env.reset()
                break

            if abbruch == True:
                MSE[i_episode] = 0.5
            else:
                MSE[i_episode] = np.mean(avg_SE)
    env.close()
    print("MAE:", MSE)
    #Mittelwert und Standardabweichung über alle Zeitschritte und Episoden zurückgegen
    return np.mean(MSE), np.std(MSE)



P,I,D=8., 16., 0.05

noiselist = np.linspace(0.0, 0.1, 11)
scorelist=[]
stdlist=[]
print("noiselist", noiselist)
for noise in noiselist:
    MSE, STD = MSE_func(P,I,D, noise)
    print("noise {:.2f} & {:.6f} $\pm$ {:.6f} \\ ".format(noise, MSE, STD))
    scorelist.append(MSE)
    stdlist.append(STD)

print("scorelist", scorelist)
print("stdlist", stdlist)