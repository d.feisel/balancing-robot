#Testen der Disturbance in der Simulation

import numpy as np
import math
import gym
import gym_foo

#Erstellen der Simulationsumgebung
env = gym.make("gym_foo:foo-v0")
spec = gym.spec("gym_foo:foo-v0")

desired_state = np.array([0, 0, 0, 0]) # gewünschter Zustands
# Gewichtung des Zustandsvektors bei Berechnung des Fehlers
desired_mask = np.array([0.02, 0.05, 0.88, 0.05])



#Verschiedene PID-Werte, deren Performance getestet werden soll
P, I, D = 8., 16., 0.05
noise_PID=0.01
state = env.reset()
integral = 0
derivative = 0
prev_error = 0
for t in range(500):
    env.render()
    state += noise_PID * np.random.randn(4)

    #Cartpole wird ab dem 100. Zeitschritt gestoßen
    if 100 <= t <= 105:
        state[3] += -170.*np.pi/180  #Die Winkelgeschwindigkeit wird für 5 aufeinanderfolgende
        #Zeitschritte erhöht
    #Berechung der Kraft bzw. der Steuergröße
    error = state - desired_state
    integral += error*env.tau
    derivative = (error - prev_error)/env.tau
    prev_error = error
    force = np.dot(P * error + I * integral + D * derivative, desired_mask)
    #Durch die Funktion env.step wird der Wagen mit der gewünschten Kraft beschleunigt
    state, reward, done, info = env.step(force)
    if done:
        env.render()
        for l in range(199):
            print("hi")
        #Ausgeben, in welchen Zeitschritt das Pendel umfällt
        print("Episode finished after {} timesteps".format(t+1))
        break
env.close()