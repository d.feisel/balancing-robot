#include <Adafruit_MPU6050.h>
#include <Adafruit_Sensor.h>
#include <Wire.h>
#include <math.h>


Adafruit_MPU6050 mpu;



int stepCounter;

//Variablen für den State
double theta;
double theta_dot;
double gravity = 9.81;
double corr = 0.0; //Korrektur für winkel, falls Oberfläche nicht exakt senkrecht zur Erdbeschleunigung steht
double gyr_corr = 0.14; //Korrektur des Gyroskops für die Winkelgeschwindigkeit
double x_factor = 89.5*3.14/(3200*1000000); //um steps in m umzurechnen
double x;
double x_prev;
double x_dot;
double state[5] = {0,0,0,0,0}; //Zustandsvektor inklusive der Zeit
double prev_state[5]; 
const int avg = 1; //Anzahl der letzten Winkel die für Mittelwertbildung benutzt werden
double last_angles[avg];
int eps = 0;

// für Zeitmessung
unsigned long begin;
unsigned long end;
float lastTime = 0; 

// für episoden des Experiments
const int eps_start = 1000;
const int eps_stop = 3000;
double MSE = 0;



double desired_mask[4]={0.02,0.05,0.88,0.05};

// Variablen für die Motorbewegung
int stepper_t = 100;
int pause = 0;

// Variablen für den PID-Algorithmus
double derivative[4];
double integral[4];
double previous_action=0;



// Ergebnisse der Simulation, die hier untersucht werden sollen
// es müssen jeweils die relevanten PID-Werte und die zugehörige force_mag auskommentiert werden

//1
//PI2 0.1
//double P = 4.28, I = 24.72,D = 2.33;
//double force_mag = 0.025;
//2
double P = 12.31, I = 7.99,D = 1.10;
double force_mag = 0.1;
//Grid search 0.1
//3
//double P = 9.29, I = 17.14,D = 0.09;
//double force_mag = 0.05;
//4
//double P = 13.57, I = 21.43,D = 0.03;
//double force_mag = 0.05;
//5
//double P = 8.00, I = 16.00,D = 0.05;
//double force_mag = 0.05;
//Grid Serach 0.5
//6
//double P = 4.00, I = 1.50,D = 0.30;
//double force_mag = 0.75;
//7
//double P = 3.00, I = 2.00,D = 0.20;
//double force_mag = 0.5;
//8
//double P = 2.50, I = 1.00,D = 0.25;
//double force_mag = 0.5;








// Funktion zur Motorsteuerung
void turn(double state[5], int steps, int dir) {
  
  // if Bedingung setzt das Signal an den Direction Pin der Schrittmotortreiber fest
  if (dir == 0) { //turn left
    digitalWrite(8, HIGH); // im Uhrzeigersinn
    digitalWrite(7, LOW); // im Uhrzeigersinn
    state[0] -= steps * x_factor; //Die x-Position wird hier anhand der Motorschritte berechnet
  }
  else { //turn right
    digitalWrite(8, LOW); // gegen den Uhrzeigersinn
    digitalWrite(7, HIGH); // gegen den Uhrzeigersinn
    state[0] += steps * x_factor;
  }

  // Schleife führt die eigentliche Bewegung durch, indem die Pins abwechselnd auf High und low gesetzt werden
  for (stepCounter = 0; stepCounter < steps; stepCounter++)
  {
    digitalWrite(6, HIGH);
    digitalWrite(5, HIGH);
    delayMicroseconds(stepper_t);
    digitalWrite(6, LOW);
    digitalWrite(5, LOW);
    delayMicroseconds(stepper_t);
  }
  delay(pause);
}



// Messung des Zustandsvektors 
void get_state(double state[5], double prev_state[5]) {
  end = micros();
  double dt = (double) (end - begin); //Zeit zwischen zwei Episoden
  state[1] = (state[0] - prev_state[0]) / (dt) * 1e6  * 0.5; //in steps/s
  sensors_event_t a, g, temp; // Auslesen des Beschleunigungssensors
  mpu.getEvent(&a, &g, &temp);
  state[2] = asin((a.acceleration.y - corr) / gravity); //°
  state[3] = g.gyro.x+gyr_corr;
  state[4] = (double) end;
  begin = micros();
  for (int i = 0; i < 5; ++i) {
    prev_state[i] = state[i];
  }

  // Mittelwertbildung über letzte Winkel, falls avg != 1, soll Rauschen und plötzliche Beschleunigungen verringern
  last_angles[eps%avg] = state[2];
  state[2] = 0;
  for (int i = 0; i < avg; ++i) {
    state[2] += last_angles[i]/avg;
  }
}




// Festlegen der Pins am Arduino und initialisieren des Sensors
void setup()
{
  Serial.begin(115200);
  while (!Serial) {
    delay(10); // will pause Zero, Leonardo, etc until serial console opens
  }
  pinMode(4, OUTPUT); // Enable (both steppers)
  
  //Stepper 1 
  pinMode(5, OUTPUT); // Step
  pinMode(7, OUTPUT); // Richtung
  
  //Stepper 2
  pinMode(6, OUTPUT); // Step
  pinMode(8, OUTPUT); // Richtung

  
  //digitalWrite(6,LOW);
  


  if (!mpu.begin()) {
    Serial.println("Failed to find MPU6050 chip");
    while (1) {
      delay(10);
    }
  }
  mpu.setAccelerometerRange(MPU6050_RANGE_2_G);
  mpu.setGyroRange(MPU6050_RANGE_250_DEG); //minimum?
  mpu.setFilterBandwidth(MPU6050_BAND_21_HZ);
  sensors_event_t a, g, temp;
  mpu.getEvent(&a, &g, &temp);
  
  
  corr = a.acceleration.y; // hiermit kann wenn bei starten des Programmes der Roboter aufrecht gehalten wird eine Kalibrierung der Winkelmessung vorgenommen werden
}




// Programmschleife
void loop()
{
  get_state(state,prev_state);

  float thisTime = millis();
  float dT = thisTime - lastTime;
  lastTime = thisTime;

  // PID Berechnungen
  for(int i=0; i<4; ++i){
    integral[i]+=state[i]*dT;
    derivative[i]=(state[i]-prev_state[i])/dT;
  }

  double pid=0;
  for(int i=0; i<4; ++i){
    pid += (P*state[i]+I*integral[i]+D*derivative[i]) * desired_mask[i];
  }

  // der wert der pid variable bestimmt hier die Anzahl der Motorschritte in der jeweiligen Episode
  int steps = (int) abs(pid) * force_mag;

  if(pid < 0){ //nach rechts fahren
   if (previous_action == 1){delay(0);}
   turn(state,steps,0);
   previous_action = 0;
  }
  else{ //nach links fahren
    if (previous_action == 0){delay(0);}
    turn(state,steps,1);
    previous_action = 1;
  }
  eps++;


  // Auslesen des MSE für das Experiment mithilfe des Seriellen Monitors
  if (eps == eps_start){
    Serial.println("Start");
    state[0] = 0;
  }
  if (eps_start < eps < eps_stop){
    for(int i = 0;i<4;i++){
      MSE += desired_mask[i]*state[i]*state[i];  
    }
  }
  if (eps == eps_stop){
    Serial.print("MSE = "); 
    Serial.println(MSE/(eps_stop-eps_start),5);
  }

  
}
