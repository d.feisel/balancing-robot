
#Quelle zur Implemenation des PID-Reglers:
#Von der Quelle übernommen ist die Berechnung der Abweichung "Error" und die Idee, die Komponenten
#des Zustandsvektors durch die Vektor a zu gewichten.
#Die Implementation der PID-Gleichung ist Eigenarbeit

import numpy as np
import math

import gym
import gym_foo

#Funktion zur Berechung des MSE des PID-Reglers
#Die Funktion gerechnet den Mittelwert und die Standartdabweichung des quadratischen
#Fehlers über mehrere Zeitschritte und Startzustände
def MSE_func(P,I,D, noise_PID):
    env = gym.make("gym_foo:foo-v0")  #Erstellen einer Simulationsumgebung

    desired_state = np.array([0, 0, 0, 0])  # gewünschter Zustands
    # Gewichtung des Zustandsvektors bei Berechnung des Fehlers
    desired_mask = np.array([0.02, 0.05, 0.88, 0.05])

    time = 500 # Anzahl der Zeitschritte in einer Episode
    N = 100 #Anzahl der Episoden
    MSE=np.zeros(N) #Array, in dem der mittlere Quadratische Fehler jeder Episoden gespeichert wird



    for i_episode in range(N):
        avg_SE=np.zeros(time) #Array, in dem der Quadratrische Fehler für alle Zeitschritte
        #einer Episoden gespeichert wird.
        state = env.reset()
        #Mithilfe der Variable Abbruch wird festgestellt, ob das Cartpole die 500 Zeitschritte
        # einer Episode balancieren kann
        abbruch = False

        integral = 0
        derivative = 0
        prev_error = 0

        for t in range(time):
            #env.render() #Graphische Darstellung der Simulation
            #Simulation des Messrauschens: Normalverteilte Änderung der Komponenten des Zustandsvektors
            #mit Standardabweichung noise_PID
            state+=noise_PID*np.random.randn(4)
            error = (state - desired_state)
            avg_SE[t]=0
            #Berechung der Quadartischen Abweichung
            for i in range(4):
                avg_SE[t]+=error[i]**2*desired_mask[i]

            #Berechung von Integral und Ableitung des Fehler mit Euler-Integration und Differenzenquotient
            #env.tau ist der Zeitschritt der Simulation
            integral += error*env.tau
            derivative = (error - prev_error)/env.tau
            prev_error = error

            #Berechung der Kraft, die den Wagen beschleunigt
            force = np.dot(P * error + I * integral + D * derivative, desired_mask)
            #Beschleunigung des Wagens mit der gewünschten Kraft
            state, reward, done, info = env.step(force)


            if done: #feststellen, ob das Cartpole mit den gegeben PID-Werten 500 Zeitschritte balanciert
                if t < (time-2):
                   abbruch = True
                state=env.reset()
                break

            if abbruch == True:
                MSE[i_episode] = 0.5
            else:
                MSE[i_episode] = np.mean(avg_SE)
    env.close()
    print("MAE:", MSE)
    #Mittelwert und Standardabweichung über alle Zeitschritte und Episoden zurückgegen
    return np.mean(MSE), np.std(MSE)






# grid search 0.1
#P,I,D=13.571428571428571,21.428571428571427,0.028571428571428574  #0.008659354792453828
#P,I,D=11.428571428571429,21.428571428571427,0.05714285714285715  #0.007513877587658548
#P,I,D=9.285714285714285,17.142857142857142,0.08571428571428572  #0.006498823148116335
P,I,D=8., 16., 0.05

#pi2 0.1
P,I,D=4.28, 24.72, 2.33
#P,I,D=12.31, 7.99, 1.10
#P,I,D = 4.2799940410049535, 24.715297096169536, 2.3331094752934938
#P,I,D = 12.314686471839792, 7.988817950732899, 1.1030271335627615

# grid search 0.5
#P,I,D=2.5, 1., 0.25
#P,I,D=3., 2., 0.2
#P,I,D=4., 1.5, 0.3 # hellbau




MSE, STD = MSE_func(P,I,D, 0)
print("{:.2f} & {:.2f} & {:.2f} & {:.6f} $\pm$ {:.6f} \\ ".format(P, I, D, MSE, STD))
