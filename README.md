# Balancing Robot - Projektpraktikum 2021

Das DQN Programm stammt von Daniel Palaio: 
https://github.com/DanielPalaio/CartPole-v0_DeepRL
In seinem Github dazu finden sich auch weitere Erklärungen und Anleitungen zur Installation der relevanten Packages.
Wir haben Änderungen in der agent und main Datei vorgenommen und diese in dem Ordner DQN hochgeladen.

Die Erstellung eines custom environments, was für unsere Zwecke nötig war, wird hier erklärt: 
https://github.com/openai/gym/blob/master/docs/creating-environments.md
Für den PID-Regler und das DQN mussten wir jeweils Änderungen in dem Custom environments vornehmen.
Diese environments sind in den jeweiligen Ordnern zu finden, jedoch wird nur die Datei foo_env.py hochgeladen. 
Die anderen für die environments nötigen dateien und ordner sind unter dem obigen link zu finden.


# Autoren
Anne Sofia Riegler 

Dorian Didier Feisel
